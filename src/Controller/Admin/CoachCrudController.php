<?php

namespace App\Controller\Admin;

use App\Entity\Coach;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
 
class CoachCrudController extends AbstractCrudController
{
    public function __construct(EntityManagerInterface $entityManager)
    {
         $this->entityManager = $entityManager;
    }

    public static function getEntityFqcn(): string
    {
        return Coach::class;
    }

    
    public function configureFields( string $pageName): iterable
    {
        
        return [
            TextField::new('name'),
            TextField::new('Team')->onlyOnIndex(),
            ChoiceField::new('Team')
                ->setChoices(function() {
                    $teams = (new TeamCrudController())->getTeams($this->entityManager);
                    foreach ($teams as $team) { 
                         $choices[$team->getName()] = $team;
                     }
                    return $choices;
                })->onlyOnForms(),
        ];
    }

    


   
}
