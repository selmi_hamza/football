<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField; 
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use App\Entity\Team;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

use symfony\Component\Routing\Attribute\Route;


class TeamCrudController extends AbstractCrudController
{
    #[isGranted('ROLE_ADMIN')]
    #[Route('/admin/team', name: 'admin.team')] 
    public static function getEntityFqcn(): string
    {
      return Team::class;
    }

   
    public function configureFields(string $pageName): iterable
    {
        return [ 
            TextField::new('name'),
            TextField::new('country'),
            TextField::new('city'),
            ImageField::new('logo')
             ->onlyOnForms()
            ->setUploadDir('assets/images/'),
        ];
    }

     
    public function getTeams(EntityManagerInterface $entityManager)
    {
        $teams = $entityManager->getRepository(Team::class)->findAll();

        if (!$teams) {
            throw $this->createNotFoundException(
                'No teams found for id '
            );
        }
 
        return $teams ;

        // or render a template
        // in the template, print things with {{ product.name }}
        // return $this->render('product/show.html.twig', ['product' => $product]);
    }
   
}
